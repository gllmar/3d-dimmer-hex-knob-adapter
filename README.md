# 3d-dimmer-hex-knob-adapter

Parametric hexagonal hole to round keyed shaft

Use to interface a DC dimmer to a proper knob

![](./media/hex-knob-adapter.drawio.png)


## Dimmer

![](./media/final_result.jpg)

![](./media/intended_used.jpg)


## Techdrawing [PDF](./hex-knob-adapter-Page.pdf)

![](./hex-knob-adapter-Page.svg)

## 3D model [STL](./hex-knob-adapter-Body.stl)